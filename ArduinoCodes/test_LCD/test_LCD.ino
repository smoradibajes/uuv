#include <LiquidCrystal.h>

const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

float ypr[3] = {0,0,0};
float depth, temp, ph, gps_latitude , gps_longitude = 0;

int program = 0;
float ref_dep, mnul_blst, mnul_rol, mnul_pitch = 0;

long timeShot = millis();;
long timeShot2 = millis();;

void setup() {
  Serial.begin(115200); 
  pinMode(8,OUTPUT);
   digitalWrite(8, LOW);
  lcd.begin(16, 2);
//  lcd.print("First line");
//  lcd.setCursor(0,1);
//  lcd.print("Second line");
}

void loop() {
    timeShot = millis();
    // recieved data : [mode, ref_dep, mnul_blst, mnul_rol, mnul_pitch] 
    // ex: 2 1.2 3.3 4.4 2.2 
    if(Serial.available() > 0){
      program = Serial.parseInt();
    }
    if(Serial.available() > 0){
      ref_dep = Serial.parseFloat();
    }
    if(Serial.available() > 0){
      mnul_blst = Serial.parseFloat();
    }
    if(Serial.available() > 0){
      mnul_rol = Serial.parseFloat();
    }
    if(Serial.available() > 0){
      mnul_pitch = Serial.parseFloat();
    }
    while (Serial.available() > 0) Serial.read(); // clear incoming serial buffer      
    
    lcd.setCursor(0,0);
    lcd.print(program);
    lcd.setCursor(3,0);
    lcd.print(ref_dep);
    lcd.setCursor(9,0);
    lcd.print(mnul_blst);
    lcd.setCursor(0,1);
    lcd.print(mnul_rol);
    lcd.setCursor(7,1);
    lcd.print(mnul_pitch);
    
   
    // send data: [yaw, roll, pitch, depth, temp, ph, gps_latitude , gps_longitude]
    Serial.print("Data: ");
    Serial.print(ypr[0] * 180/M_PI); // yaw
    Serial.print(" ");
    Serial.print(ypr[1] * 180/M_PI); // roll
    Serial.print(" ");
    Serial.print(ypr[2] * 180/M_PI); // pitch
    Serial.print(" ");
    Serial.print(depth);
    Serial.print(" ");
    Serial.print(temp);
    Serial.print(" ");
    Serial.print(ph);
    Serial.print(" ");
    Serial.print(gps_latitude);
    Serial.print(" ");
    Serial.println(gps_longitude);
    int del_ay = (100-(millis() - timeShot));
    if (del_ay>0)
    {
      delay(del_ay); // T_s = 0.1 s 
    }
}
