#include <Arduino.h>
#include "BasicStepperDriver.h"                 // StepperDriver library from https://github.com/laurb9/StepperDriver 

#define MOTOR_STEPS 200.0                       // Motor steps per revolution
#define FACTOR 2                                // Increase this number if you want the motor to spin faster    // 2 was good
#define OUT_RPM 60                              // RPM at output shaft. Change this value to desired output     // DEFAULT WAS 60, 90 was fast
#define GEAR_RATIO 5.18                         // Motor gearbox ratio = 5.18
#define PULLEY_RATIO 3                          // Pulley system ratio = 3             
#define MOTOR_RPM OUT_RPM*GEAR_RATIO/FACTOR     // RPM at the motor. This number seems to be limited to 200
#define MICROSTEPS 1*FACTOR                     // Micrstop should be 2, but multiplied by a factor of 2
// Acceleration and deceleration values are always in FULL steps / s^2
#define MOTOR_ACCEL 400
#define MOTOR_DECEL 400

// Ballast motor (propulsion) (right)
#define BLST_EN 11
#define BLST_STP 12
#define BLST_DIR 13
#define BLST_ENDSTOP 18

// Define maximum range for ballast (in steps)
#define BLST_MAX 12.0*MOTOR_STEPS*MICROSTEPS*GEAR_RATIO/FACTOR                           // max is 16 revolutions                                             

BasicStepperDriver stepperBlst(MOTOR_STEPS, BLST_DIR, BLST_STP, BLST_EN);

float blstPos = 0;


void blstMotor_step(float m);
void homeBlst();

long timeShot = millis();
long timeShot2 = millis();
int del_ay;
float t_dure;
int isrTime;

/*-----------------------new------------------------*/
bool blstMotor_home_state = false;
int blst_direction; // 1 opens the pistons, -1 closing the pistons and 0 stop
#define BLST_LEAD 0.008 // Distance travelled by screw in 1 revolution 
#define BLST_STEP_DISTANCE BLST_LEAD/(MOTOR_STEPS*MICROSTEPS*GEAR_RATIO/FACTOR) // travelled distance of one step 

float rho = 1.03e+3;
float h_max = 12 * BLST_LEAD; // 12 rotations * 0.008 meter per 1 revoloution
float A_cyl = 3.1416 *(0.036/2)*(0.036/2)*3;
float V_max = h_max * A_cyl;
float m_max = V_max * rho;
float m_cyl; // cylinder mass
float m_a_step = BLST_STEP_DISTANCE * A_cyl * rho;

// -------- new

///*-----------------------timer interupt------------------------*/
//void setupTheInterupt(){
//  cli();//stop interrupts
//
// //set timer4 interrupt at 100Hz
// TCCR4A = 0;// set entire TCCR1A register to 0
// TCCR4B = 0;// same for TCCR1B
// TCNT4  = 0;//initialize counter value to 0
// // set compare match register for 1hz increments
// OCR4A = 15624/10;// = (16*10^6) / (100*1024) - 1 (must be <65536)
// // turn on CTC mode
// TCCR4B |= (1 << WGM12);
// // Set CS12 and CS10 bits for 1024 prescaler
// TCCR4B |= (1 << CS12) | (1 << CS10);  
// // enable timer compare interrupt
// TIMSK4 |= (1 << OCIE4A);
//
//sei();//allow interrupts
//}


//ISR(TIMER4_COMPA_vect){     //timer1 interrupt 100Hz 
//  
//  unsigned wait_time_micros = stepperBlst.nextAction(); 
//  if (!digitalRead(BLST_ENDSTOP) && blstMotor_home_state) {
//    Serial.println("Ballast motor homed");
//    stepperBlst.stop();
//    blstPos = 0;
//    blstMotor_home_state = false;
//  }
//  if (digitalRead(BLST_ENDSTOP) && !blstMotor_home_state) {
//    blstMotor_home_state = true;
//    Serial.println("Ballast motor NOT homed");
//  } 
//  isrTime = millis()-timeShot2;
//}


void setup() {
  Serial.begin(115200);   // start the Serial monitor with speed of 115200 Bauds
  // put your setup code here, to run once:
  pinMode(BLST_ENDSTOP, INPUT_PULLUP);
  stepperBlst.begin(MOTOR_RPM, MICROSTEPS);
  stepperBlst.setEnableActiveState(LOW);
  stepperBlst.enable();    // enable motor
  homeBlst();
//  setupTheInterupt();

}

void loop() {
  timeShot = millis();
  if ( (millis()- timeShot2)>99){ // Ts=0.1s
    t_dure = (millis() - timeShot2) / 1000.0; // duration of each loop
    timeShot2 = millis();
    // put your main code here, to run repeatedly:
    m_cyl = blstPos * A_cyl * rho;
    if (Serial.available() > 0) {
          blstMotor_step(Serial.parseFloat());
      while (Serial.available() > 0) Serial.read(); // clear incoming serial buffer
    }
  //  blstMotor(0.01);
    Serial.println(".......................................................... ");
    Serial.println(t_dure);
    Serial.println(blstPos);
    Serial.println(m_cyl);
//    Serial.println(isrTime);
    del_ay = (100 - (millis() - timeShot));
    
  } // end of the if for Ts = 0.1
  
  // nonblocking movement for the blst stepper motor
  if (!digitalRead(BLST_ENDSTOP) && blst_direction == -1) { // safty switch is on, so stop motor going further
//    Serial.println("Ballast motor homed");
    stepperBlst.stop();
    blstPos = 0;
    m_cyl = 0;
    blst_direction = 0;
    blstMotor_home_state = false;
  } else if( blstPos >= h_max && blst_direction == 1){ // motor at max position so stop the motor 
    stepperBlst.stop();
    blstPos = h_max;
    m_cyl = m_max;
    blst_direction = 0;
    blstMotor_home_state = true;
  }
  if (digitalRead(BLST_ENDSTOP) && !blstMotor_home_state) {
    blstMotor_home_state = true;
//    Serial.println("Ballast motor NOT homed");
  } 
  unsigned wait_time_micros = stepperBlst.nextAction(); 
  blstPos = blstPos + blst_direction*BLST_STEP_DISTANCE;
  
//  unsigned wait_time_micros = stepperBlst.nextAction();
//  del_ay = (100 - (millis() - timeShot)); // T_s = 0.1 s
//  if (del_ay > 0)
//  {
//    delay(del_ay);
//  }

}



void blstMotor_step(float m) {
  stepperBlst.enable();    // enable motor
  stepperBlst.setSpeedProfile(stepperBlst.LINEAR_SPEED, MOTOR_ACCEL, MOTOR_DECEL);
  if(m>m_max){
    m = m_max;
  }
  if(m<0){
    m = 0;
  }
  if(abs(m_cyl-m)>m_a_step){
    float blst_steps = (m - m_cyl) /(A_cyl * rho * BLST_STEP_DISTANCE);
    float blst_angle = blst_steps/(MOTOR_STEPS * MICROSTEPS * GEAR_RATIO) * FACTOR * 360;
    if (m_cyl < m){
      stepperBlst.startRotate(blst_angle * GEAR_RATIO / FACTOR);
      blst_direction = 1;
    }
    else if (m_cyl>m){
      stepperBlst.startRotate(blst_angle * GEAR_RATIO / FACTOR);
      blst_direction = -1;
    }
  }
}

void homeBlst() {
  stepperBlst.enable();    // enable motor
  stepperBlst.setSpeedProfile(stepperBlst.LINEAR_SPEED, MOTOR_ACCEL, MOTOR_DECEL);
  stepperBlst.startMove(-BLST_MAX * MOTOR_STEPS * MICROSTEPS);

  unsigned long startTime = millis();

  // first, check if the AUV is already homed
  // digitalRead(BLST_ENDSTOP) = 0 if AUV is already homed, digitalRead(BLST_ENDSTOP) = 1 if AUV is not homed
  if (!digitalRead(BLST_ENDSTOP)) {
    Serial.println("Ballast motor already homed");
    stepperBlst.stop();
  } else {
    Serial.println("Homing ballast motor...");
    while (1) {
      unsigned long currentTime = millis();
      unsigned long elapsedTime = currentTime - startTime;
      if (!digitalRead(BLST_ENDSTOP)) {
        stepperBlst.stop();
        blstPos = 0;
        Serial.println("Ballast motor homed\n");
        break;
      } else if (elapsedTime > 25000) {
        stepperBlst.stop();
        Serial.println("Ballast motor timed out\n");
        Serial.println(elapsedTime);
        break;
      }
      // motor control loop - send pulse and return how long to wait until next pulse
      unsigned wait_time_micros = stepperBlst.nextAction();
    }
  }
}
