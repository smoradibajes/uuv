
void homeAll() {
  homeBlst();
  delay(500);
  homePitch();
  delay(500);
  homeRoll();
}

void homeRoll() {
  stepperRoll.enable();    // enable motor
  stepperRoll.setSpeedProfile(stepperRoll.LINEAR_SPEED, MOTOR_ACCEL, MOTOR_DECEL);
  stepperRoll.startMove(-ROLL_MAX * MOTOR_STEPS * MICROSTEPS);

  unsigned long startTime = millis();

  // first, check if the AUV is already homed
  // digitalRead(ROLL_ENDSTOP) = 0 if AUV is already homed, digitalRead(ROLL_ENDSTOP) = 1 if AUV is not homed
  if (!digitalRead(ROLL_ENDSTOP)) {
    Serial.println("Roll motor already homed");
    stepperRoll.stop();
  } else {
    Serial.println("Homing roll motor...");
    while (1) {
      unsigned long currentTime = millis();
      unsigned long elapsedTime = currentTime - startTime;
      if (!digitalRead(ROLL_ENDSTOP)) {
        stepperRoll.stop();
        rollPos = 0;
        Serial.println("Roll motor homed\n");
        break;
      } else if (elapsedTime > 5000) {
        stepperRoll.stop();
        Serial.println("Roll motor timed out\n");
        Serial.println(elapsedTime);
        break;
      }
      // motor control loop - send pulse and return how long to wait until next pulse
      unsigned wait_time_micros = stepperRoll.nextAction();
    }
  }
}

void homePitch() {
  stepperPitch.enable();    // enable motor
  stepperPitch.setSpeedProfile(stepperPitch.LINEAR_SPEED, MOTOR_ACCEL, MOTOR_DECEL);
  stepperPitch.startMove(PITCH_MAX * MOTOR_STEPS * MICROSTEPS);

  unsigned long startTime = millis();

  // first, check if the AUV is already homed
  // digitalRead(PITCH_ENDSTOP) = 0 if AUV is already homed, digitalRead(PITCH_ENDSTOP) = 1 if AUV is not homed
  if (!digitalRead(PITCH_ENDSTOP)) {
    Serial.println("Pitch motor already homed");
    stepperPitch.stop();
  } else {
    Serial.println("Homing pitch motor...");
    while (1) {
      unsigned long currentTime = millis();
      unsigned long elapsedTime = currentTime - startTime;
      if (!digitalRead(PITCH_ENDSTOP)) {
        stepperPitch.stop();
        pitchPos = 0;
        Serial.println("Pitch motor homed\n");
        break;
      } else if (elapsedTime > 30000) {
        stepperPitch.stop();
        Serial.println("Pitch motor timed out\n");
        Serial.println(elapsedTime);
        break;
      }
      // motor control loop - send pulse and return how long to wait until next pulse
      unsigned wait_time_micros = stepperPitch.nextAction();
    }
  }
}

void homeBlst() {
  stepperBlst.enable();    // enable motor
  stepperBlst.setSpeedProfile(stepperBlst.LINEAR_SPEED, MOTOR_ACCEL, MOTOR_DECEL);
  stepperBlst.startMove(-BLST_MAX * MOTOR_STEPS * MICROSTEPS);

  unsigned long startTime = millis();

  // first, check if the AUV is already homed
  // digitalRead(BLST_ENDSTOP) = 0 if AUV is already homed, digitalRead(BLST_ENDSTOP) = 1 if AUV is not homed
  if (!digitalRead(BLST_ENDSTOP)) {
    Serial.println("Ballast motor already homed");
    stepperBlst.stop();
  } else {
    Serial.println("Homing ballast motor...");
    while (1) {
      unsigned long currentTime = millis();
      unsigned long elapsedTime = currentTime - startTime;
      if (!digitalRead(BLST_ENDSTOP)) {
        stepperBlst.stop();
        blstPos = 0;
        Serial.println("Ballast motor homed\n");
        break;
      } else if (elapsedTime > 25000) {
        stepperBlst.stop();
        Serial.println("Ballast motor timed out\n");
        Serial.println(elapsedTime);
        break;
      }
      // motor control loop - send pulse and return how long to wait until next pulse
      unsigned wait_time_micros = stepperBlst.nextAction();
    }
  }
}
