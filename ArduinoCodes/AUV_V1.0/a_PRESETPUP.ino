// REMEBER TO DISCONNECT PIN 0 & 1 IF UPLOADING NEW SKETCH
/* Include libraries -------------------------------------------------------------------------------------*/
#include <Arduino.h>
#include <Wire.h>

// ---------------------------------new 2021-----------------
#include <math.h>       /* sin */

//#define PI 3.14159265

#define BLST_MAX_ROT 13 // maximum 13 revolution

// ---------------------------------end of new 2021-----------------

#include "BasicStepperDriver.h"                 // StepperDriver library from https://github.com/laurb9/StepperDriver 

#include "I2Cdev.h"                             // I2Cdev and MPU6050 library from https://github.com/jrowberg/i2cdevlib
#include "MPU6050_6Axis_MotionApps20.h"
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
#include "Wire.h"
#endif

#include <NeoSWSerial.h>                        // NeoSWSerial library https://github.com/SlashDevin/NeoSWSerial
#include <TinyGPS++.h>                          // TinyGPS++ library for converting GPS NMEA data to useful info https://github.com/mikalhart/TinyGPSPlus
NeoSWSerial gpsSerial(53, 52);                  // (Rx, Tx) Arduino pins. RX pin is white, TX pin is orange
TinyGPSPlus gps;                                // Create a TinyGPS++ object

/* Define variables for MPU6050 ----------------------------------------------------------------------- */
MPU6050 mpu;
#define OUTPUT_READABLE_YAWPITCHROLL
#define INTERRUPT_PIN 19
// SDA: blue wire, SCL: oragne wire
bool blinkState = false;
// MPU control/status vars
bool dmpReady = false;  // set true if DMP init was successful
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

// orientation/motion vars
Quaternion q;           // [w, x, y, z]         quaternion container
VectorInt16 aa;         // [x, y, z]            accel sensor measurements
VectorInt16 aaReal;     // [x, y, z]            gravity-free accel sensor measurements
VectorInt16 aaWorld;    // [x, y, z]            world-frame accel sensor measurements
VectorFloat gravity;    // [x, y, z]            gravity vector
float euler[3];         // [psi, theta, phi]    Euler angle container
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector

// interrupt detection routine
volatile bool mpuInterrupt = false;     // indicates whether MPU interrupt pin has gone high
void dmpDataReady() {
  mpuInterrupt = true;
}

/* Define variables for StepperDriver library -----------------------------------------------------------------------------*/
#define MOTOR_STEPS 200.0                       // Motor steps per revolution
#define FACTOR 2                                // Increase this number if you want the motor to spin faster    // 2 was good
#define OUT_RPM 90                              // RPM at output shaft. Change this value to desired output     // DEFAULT WAS 60, 90 was fast
#define GEAR_RATIO 5.18                         // Motor gearbox ratio = 5.18
#define PULLEY_RATIO 3                          // Pulley system ratio = 3             
#define MOTOR_RPM OUT_RPM*GEAR_RATIO/FACTOR     // RPM at the motor. This number seems to be limited to 200
#define MICROSTEPS 1*FACTOR                     // Micrstop should be 2, but multiplied by a factor of 2
// Acceleration and deceleration values are always in FULL steps / s^2
#define MOTOR_ACCEL 400
#define MOTOR_DECEL 400

/* Set up for stepper motors and endstops  --------------------------------------------------------------------------------*/
// Roll motor (rolling weights) (middle)
#define ROLL_EN 8                               // Enable pin (white wire)
#define ROLL_STP 9                              // Step pin (orange wire)
#define ROLL_DIR 10                             // Direction pin (blue wire)
#define ROLL_ENDSTOP 3                          // Endstop (yellow wire)
// Endstop pin needs to be an interrupt pin. On Arduino Mega, use one of the pins: 2, 3, 18, 19, 20, 21

// Pitch motor (moving weights) (left)
#define PITCH_EN 5
#define PITCH_STP 6
#define PITCH_DIR 7
#define PITCH_ENDSTOP 2

// Ballast motor (propulsion) (right)
#define BLST_EN 11
#define BLST_STP 12
#define BLST_DIR 13
#define BLST_ENDSTOP 18

// Define maximum range for roll, pitch, ballast (in steps)
#define ROLL_MAX 350.0/360*MOTOR_STEPS*MICROSTEPS*GEAR_RATIO/FACTOR*PULLEY_RATIO
float PITCH_MAX = 140.0 * MOTOR_STEPS * MICROSTEPS / FACTOR;                             // max is 140 revolutions
#define BLST_MAX BLST_MAX_ROT*MOTOR_STEPS*MICROSTEPS*GEAR_RATIO/FACTOR                           // max is 13 revolutions                                             

BasicStepperDriver stepperRoll(MOTOR_STEPS, ROLL_DIR, ROLL_STP, ROLL_EN);
BasicStepperDriver stepperPitch(MOTOR_STEPS, PITCH_DIR, PITCH_STP, PITCH_EN);
BasicStepperDriver stepperBlst(MOTOR_STEPS, BLST_DIR, BLST_STP, BLST_EN);

// current position of roll, ballast, pitch motors
float rollPos = 0, pitchPos = 0, blstPos = 0;

// Funciton prototypes
void homeAll();
void homeRoll();
void homePitch();
void homeBlst();

void gpsInfo();
void displayInfo();
void imuInfo();
void imuSetup();

void rollMotor(float roll_angle);
void pitchMotor(float pitch_rot);
void blstMotor(float blst_rot);

void(* resetFunc) (void) = 0;                  // Declare reset function at address 0


/*---------------------------------------------2021----------------------------------------------------------------------*/

void mnul_control();

/*-----------------------usb variables------------------------*/

int program = 2;
float ref_dep, mnul_blst, mnul_rol, mnul_pitch;
float depth, temp = 20, ph = 7;
float gps_latitude = -37.7995239;
float gps_longitude = 144.9613464;


/*-----------------------depth conroller------------------------*/

// system model
// from matlab:
// bd = [0,0.003378451078302,-0.012399070264330,0.009018855807022]
// ad = [1,-2.870206431012961,2.741305156205054,-0.871098691745799]
float b0 = 0;
float b1 = 0.003378451078302;
float b2 = -0.012399070264330;
float b3 = 0.009018855807022;
float a0 = 1;
float a1 = -2.870206431012961;
float a2 = 2.741305156205054;
float a3 = -0.871098691745799;

// state variables
// the numbers shows the delay, ex: x0 is the current x, x1 was the x0 last loop
float x0, x1, x2, x3, x_temp; // states
float u0, u1, u2, u3, u_temp; // inputs
float y_est; // estimator output
float e, integral, u_ctrl; // error signal, integrator and controller effort in controller

// state feedback gains
// [-2.438716950915433e-05,-0.031714597708034,0.062768365567298,-0.031053761586178]
float ki = -2.438716950915433e-5;
float k1 = -0.031714597708034;
float k2 = 0.062768365567298;
float k3 = -0.031053761586178;

// feedforward gains
// [-5.670930620534624e+05;-5.670930620534624e+05;-5.670930620534624e+05;-0.018967160954378]
float N1 = -5.670930620534624e+05;
float N2 = -5.670930620534624e+05;
float N3 = -5.670930620534624e+05;
float N4 = -0.018967160954378;

// Observer gains
// [-2.730342713182523e+04;-2.728616272772625e+04;-2.726671331493510e+04]
float L1 = -2.730342713182523e+04;
float L2 = -2.728616272772625e+04;
float L3 = -2.726671331493510e+04;

// Funciton prototype

void depthControl();
void estimator();
void blstMotor_step(float m); // move the ballast stepper motor

// stepper 

/*-----------------------stepper  nonblocking variables------------------------*/

bool blstMotor_home_state = false;
int blst_direction; // 1 opens the pistons, -1 closing the pistons and 0 stop
#define BLST_LEAD 0.008 // Distance travelled by screw in 1 revolution 
#define BLST_STEP_DISTANCE BLST_LEAD/(MOTOR_STEPS*MICROSTEPS*GEAR_RATIO/FACTOR) // travelled distance of one step 

float rho = 1.03e+3;
float h_max = BLST_MAX_ROT * BLST_LEAD; // 13 rotations * 0.008 meter per 1 revoloution
float A_cyl = PI *(0.036/2)*(0.036/2)*3;
float V_max = h_max * A_cyl;
float m_max = V_max * rho;
float m_cyl; // cylinder mass
float m_a_step = BLST_STEP_DISTANCE * A_cyl * rho;

// time captures

long timeShot = millis(); // to compute the 
long timeShot2 = millis(); // used to make sure Ts stays 0.1 sec

int del_ay;
float t_dure;
/*-----------------------sensors------------------------*/

int sensorPin = A1; // select the analog input pin for the pH sensor
int sensorValue = 0;   // variable to store the value coming from the sensor
float ad7 = 300.0;     // change this value to the one on serial monitor when in pH7 buffer
float ad4 = 402.0;     // change this value to the one on serial monitor when in pH4 buffer

int currentValue = 0;
int ph_counter = 0;

//Pressure sensor at A0

const float OffSet = 0.483;
float V,P;
//int pressurePin = 0;

// Funciton prototype
void Get_pH(); 
void Get_Pressure();
