void loop() {
  if ( (millis()- timeShot)>99){ // Ts=0.1s 
    t_dure = (millis() - timeShot) / 1000.0; // duration of each loop
    timeShot = millis();
    m_cyl = blstPos * A_cyl * rho;
    // read critical sensors first
    Get_Pressure(); // update depth
    imuInfo(); // update ypr which has pitch, yaw and roll
  
    // in each loop compute the controller effort
    depthControl();
    
    
    switch (program) {
      case 0:
  //      Serial.println("Status: mnul control \n");
        blstMotor_step(mnul_blst);
        break;
      case 1:
  //      Serial.println("Status: Homing all motors... \n");
        homeAll();
        break;
      case 2:
  //      Serial.println("Status: depth control \n");
        blstMotor_step((u_ctrl + (m_max/2)));
        break;
      case 3:
  //      Serial.println("Status: Disabling motors... \n");
        // disable motor, remeber to enable motor (option 'e') later
        stepperRoll.disable(); stepperPitch.disable(); stepperBlst.disable();
        break;
      case 4:
  //      Serial.println("Status: Enabling motors... \n");
        // enable motor
        stepperRoll.enable(); stepperPitch.enable(); stepperBlst.enable();
        break;
      case 5:
        
        break;
      case -1:
        resetFunc();
        break;
      default:
  //      Serial.println("Error: Choose again.\n");
        break;
    }
  
    
  //  Serial.println("Please select program: ");
  //  Serial.println("h. Home");
  //  Serial.println("g. Display GPS info");
  //  Serial.println("i. Display IMU info");
  //  Serial.println("m. Manual control");
  //  Serial.println("d. Disable motor");
  //  Serial.println("e. Enable motor");
  //  Serial.println("r. Reset");
  
  //  while (Serial.available() > 0) Serial.read(); // clear incoming serial buffer
  //  while (!Serial.available()) {}
  
    
    // recieved data : [mode, ref_dep, mnul_blst, mnul_rol, mnul_pitch] 
    // ex: 2 1.2 3.3 4.4 2.2 
    if(Serial.available() > 0){
      program = Serial.parseInt();
    }
    if(Serial.available() > 0){
      ref_dep = Serial.parseFloat();
    }
    if(Serial.available() > 0){
      mnul_blst = Serial.parseFloat();
    }
    if(Serial.available() > 0){
      mnul_rol = Serial.parseFloat();
    }
    if(Serial.available() > 0){
      mnul_pitch = Serial.parseFloat();
    }
    while (Serial.available() > 0) Serial.read(); // clear incoming serial buffer
  
    
    // read other sensors
    gpsInfo();
  //  temp;
  //  ph
    
    // update the estimator with the chosen input
    estimator();
    
    // send data: [yaw, roll, pitch, depth, temp, ph, gps_latitude , gps_longitude, t_dure, u_ctrl, m_cyl, program]
    Serial.print("Data: ");
    Serial.print(ypr[0] * 180/M_PI); // yaw
    Serial.print(" ");
    Serial.print(ypr[1] * 180/M_PI); // roll
    Serial.print(" ");
    Serial.print(ypr[2] * 180/M_PI); // pitch
    Serial.print(" ");
    Serial.print(depth);
    Serial.print(" ");
    Serial.print(temp);
    Serial.print(" ");
    Serial.print(ph);
    Serial.print(" ");
    Serial.print(gps_latitude);
    Serial.print(" ");
    Serial.print(gps_longitude);
    Serial.print(" ");
    Serial.print(t_dure);
    Serial.print(" ");
    Serial.print(u_ctrl);
    Serial.print(" ");
    Serial.print(m_cyl);
    Serial.print(" ");
    Serial.println(program);
//    del_ay = (100-(millis() - timeShot)); // T_s = 0.1 s 
//    if (del_ay>0)
//    {
//      delay(del_ay);
//    }
    
  } // end of the if for Ts = 0.1

  // nonblocking movement for the blst stepper motor
  if (!digitalRead(BLST_ENDSTOP) && blst_direction == -1) { // safty switch is on, so stop motor going further
//    Serial.println("Ballast motor homed");
    stepperBlst.stop();
    blstPos = 0;
    m_cyl = 0;
    blst_direction = 0;
    blstMotor_home_state = false;
  } else if( blstPos >= h_max && blst_direction == 1){ // motor at max position so stop the motor 
    stepperBlst.stop();
    blstPos = h_max;
    m_cyl = m_max;
    blst_direction = 0;
    blstMotor_home_state = true;
  }
  if (digitalRead(BLST_ENDSTOP) && !blstMotor_home_state) {
    blstMotor_home_state = true;
//    Serial.println("Ballast motor NOT homed");
  } 
  unsigned wait_time_micros = stepperBlst.nextAction(); 
  if (wait_time_micros == 0){
    blst_direction = 0;
  }
  blstPos = blstPos + blst_direction*BLST_STEP_DISTANCE;
  
}
