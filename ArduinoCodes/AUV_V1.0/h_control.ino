void depthControl(){
  e = ref_dep - depth;
  integral = integral + e;
  u_ctrl = (ref_dep*N1 - x1)*k1 + (ref_dep*N2 - x2)*k2 + (ref_dep*N3 - x3)*k3 + (integral)*ki ;
}




void estimator(){
  float u = m_cyl - (m_max/2);
  float y = depth;
  x3 = x2 + L3*(y - y_est);
  x2 = x1 + L2*(y - y_est);
  x1 = x0 + L1*(y - y_est);
  x0 = u + (-a1*x1 - a2*x2 - a3*x3)/a0;
  y_est = b0*x0 + b1*x1 + b2*x2 + b3*x3;  
}


void blstMotor_step(float m) {
  stepperBlst.enable();    // enable motor
  stepperBlst.setSpeedProfile(stepperBlst.LINEAR_SPEED, MOTOR_ACCEL, MOTOR_DECEL);
  if(m>m_max){
    m = m_max;
  }
  if(m<0){
    m = 0;
  }
  if(abs(m_cyl-m)>m_a_step){
    float blst_steps = (m - m_cyl) /(A_cyl * rho * BLST_STEP_DISTANCE);
    float blst_angle = blst_steps/(MOTOR_STEPS * MICROSTEPS * GEAR_RATIO) * FACTOR * 360;
    if (m_cyl < m){
      stepperBlst.startRotate(blst_angle * GEAR_RATIO / FACTOR);
      blst_direction = 1;
    }
    else if (m_cyl>m){
      stepperBlst.startRotate(blst_angle * GEAR_RATIO / FACTOR);
      blst_direction = -1;
    }
  }
}

// --------incomplete-------------
void rollMotor_step(float roll_angle) {
  float roll_steps = -roll_angle / 360 * MOTOR_STEPS * MICROSTEPS * GEAR_RATIO / FACTOR;
  rollPos += roll_angle;

  if (-roll_steps <= ROLL_MAX && rollPos <= 350 && rollPos >= 0) {  // motor within range
    stepperRoll.rotate(roll_angle * PULLEY_RATIO * GEAR_RATIO / FACTOR);
  } else {                                          // motor out of range
    rollPos += roll_angle;
  }
}

void pitchMotor_step(float pitch_rot) {
  // maximum is 140 rotations!
  float pitch_angle = -360.0 * pitch_rot;    // 70 rotations
  float pitch_steps = -pitch_angle / 360 * MOTOR_STEPS * MICROSTEPS / FACTOR;
  pitchPos += pitch_rot;

  if (pitch_steps <= PITCH_MAX && pitchPos <= 140 && pitchPos >= 0) {                   // motor within range
    stepperPitch.rotate(pitch_angle / FACTOR);
  } else {                                        // motor out of range
    pitchPos -= pitch_rot;
  }
}
