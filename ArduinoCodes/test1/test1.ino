// REMEBER TO DISCONNECT PIN 0 & 1 IF UPLOADING NEW SKETCH
/* Include libraries -------------------------------------------------------------------------------------*/
#include <Arduino.h>
#include <Wire.h>

//#include "BasicStepperDriver.h"                 // StepperDriver library from https://github.com/laurb9/StepperDriver 
#include <Stepper.h>

//#include "I2Cdev.h"                             // I2Cdev and MPU6050 library from https://github.com/jrowberg/i2cdevlib
//#include "MPU6050_6Axis_MotionApps20.h"
//#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
//#include "Wire.h"
//#endif

//#include <NeoSWSerial.h>                        // NeoSWSerial library https://github.com/SlashDevin/NeoSWSerial
//#include <TinyGPS++.h>                          // TinyGPS++ library for converting GPS NMEA data to useful info https://github.com/mikalhart/TinyGPSPlus
//NeoSWSerial gpsSerial(53, 52);                  // (Rx, Tx) Arduino pins. RX pin is white, TX pin is orange
//TinyGPSPlus gps;                                // Create a TinyGPS++ object

/* Define variables for StepperDriver library -----------------------------------------------------------------------------*/
#define MOTOR_STEPS 200.0                       // Motor steps per revolution
#define FACTOR 2                                // Increase this number if you want the motor to spin faster    // 2 was good
#define OUT_RPM 60                              // RPM at output shaft. Change this value to desired output     // DEFAULT WAS 60, 90 was fast
#define GEAR_RATIO 5.18                         // Motor gearbox ratio = 5.18
#define PULLEY_RATIO 3                          // Pulley system ratio = 3             
#define MOTOR_RPM OUT_RPM*GEAR_RATIO/FACTOR     // RPM at the motor. This number seems to be limited to 200
#define MICROSTEPS 1*FACTOR                     // Micrstop should be 2, but multiplied by a factor of 2
// Acceleration and deceleration values are always in FULL steps / s^2
#define MOTOR_ACCEL 400
#define MOTOR_DECEL 400

/* Set up for stepper motors and endstops  --------------------------------------------------------------------------------*/
// Roll motor (rolling weights) (middle)
#define ROLL_EN 8                               // Enable pin (white wire)
#define ROLL_STP 9                              // Step pin (orange wire)
#define ROLL_DIR 10                             // Direction pin (blue wire)
#define ROLL_ENDSTOP 11                         // Endstop (yellow wire)
// Endstop pin needs to be an interrupt pin. On Arduino Mega, use one of the pins: 2, 3, 18, 19, 20, 21

// Define maximum range for roll, pitch, ballast (in steps)
#define ROLL_MAX 350.0/360*MOTOR_STEPS*MICROSTEPS*GEAR_RATIO/FACTOR*PULLEY_RATIO
float PITCH_MAX = 140.0 * MOTOR_STEPS * MICROSTEPS / FACTOR;                             // max is 140 revolutions
#define BLST_MAX 16.0*MOTOR_STEPS*MICROSTEPS*GEAR_RATIO/FACTOR                           // max is 16 revolutions                                             

BasicStepperDriver stepperRoll(MOTOR_STEPS, ROLL_DIR, ROLL_STP, ROLL_EN);

void rollMotor(float roll_angle);


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);   // start the Serial monitor with speed of 9600 Bauds
  pinMode(ROLL_ENDSTOP, INPUT_PULLUP);
  stepperRoll.begin(MOTOR_RPM / 2, MICROSTEPS);  
  stepperRoll.setEnableActiveState(LOW);
  stepperRoll.enable();    // enable motor

}

void loop() {
  // put your main code here, to run repeatedly:
  rollMotor(90.0);
  delay(6000);
}
float rollPos = 0;

void rollMotor(float roll_angle) {
  float roll_steps = -roll_angle / 360 * MOTOR_STEPS * MICROSTEPS * GEAR_RATIO / FACTOR;
  rollPos += roll_angle;

  if (-roll_steps <= ROLL_MAX && rollPos <= 350 && rollPos >= 0) {  // motor within range
    stepperRoll.rotate(roll_angle * PULLEY_RATIO * GEAR_RATIO / FACTOR);
    Serial.print("Rolling ");
    Serial.print(roll_angle);
    Serial.println(" degrees...\n");
  } else {                                          // motor out of range
    rollPos += roll_angle;
    Serial.println("Out of range. Home again.\n");
  }
  Serial.print("Current roll motor angle: ");
  Serial.print(rollPos);
  Serial.print(" degrees\n");
}
