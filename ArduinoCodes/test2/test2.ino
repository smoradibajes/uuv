/* Example sketch to control a 28BYJ-48 stepper motor with ULN2003 driver board and Arduino UNO. More info: https://www.makerguides.com */

// Include the Arduino Stepper.h library:
#include <Stepper.h>
#include<stdio.h>
// Define number of steps per rotation:
const int stepsPerRevolution = 2048;

// Wiring:
// Pin 8 to IN1 on the ULN2003 driver
// Pin 9 to IN2 on the ULN2003 driver
// Pin 10 to IN3 on the ULN2003 driver
// Pin 11 to IN4 on the ULN2003 driver
#define IN1 8
#define IN2 9
#define IN3 10
#define IN4 11

// Create stepper object called 'myStepper', note the pin order:
Stepper myStepper = Stepper(stepsPerRevolution, 8, 10, 9, 11);

void setup() {
  // Set the speed to 5 rpm:
  myStepper.setSpeed(12);
  
  // Begin Serial communication at a baud rate of 9600:
  Serial.begin(9600);
//  myStepper.step(stepsPerRevolution);
//  myStepper.step(-30);
}

void loop() {
  while (Serial.available() > 0) Serial.read(); // clear incoming serial buffer
  while (!Serial.available()) {}
  
  // read the incoming 2 byte:
  int rotSpeed = Serial.parseInt();
  Serial.println(rotSpeed);
  while (!Serial.available()) {}
  int rotSteps = Serial.parseInt();
  Serial.println(rotSteps);
  while (Serial.available() > 0) Serial.read(); // clear incoming serial buffer
  myStepper.setSpeed(rotSpeed);
  myStepper.step(rotSteps);
  
}
