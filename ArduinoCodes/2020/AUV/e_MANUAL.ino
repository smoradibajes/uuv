void rollMotor(float roll_angle) {
  float roll_steps = -roll_angle / 360 * MOTOR_STEPS * MICROSTEPS * GEAR_RATIO / FACTOR;
  rollPos += roll_angle;

  if (-roll_steps <= ROLL_MAX && rollPos <= 350 && rollPos >= 0) {  // motor within range
    stepperRoll.rotate(roll_angle * PULLEY_RATIO * GEAR_RATIO / FACTOR);
    Serial.print("Rolling ");
    Serial.print(roll_angle);
    Serial.println(" degrees...\n");
  } else {                                          // motor out of range
    rollPos += roll_angle;
    Serial.println("Out of range. Home again.\n");
  }
  Serial.print("Current roll motor angle: ");
  Serial.print(rollPos);
  Serial.print(" degrees\n");
}

void pitchMotor(float pitch_rot) {
  // maximum is 140 rotations!
  float pitch_angle = -360.0 * pitch_rot;    // 70 rotations
  float pitch_steps = -pitch_angle / 360 * MOTOR_STEPS * MICROSTEPS / FACTOR;
  pitchPos += pitch_rot;

  if (pitch_steps <= PITCH_MAX && pitchPos <= 140 && pitchPos >= 0) {                   // motor within range
    stepperPitch.rotate(pitch_angle / FACTOR);
    Serial.print("Rolling ");
    Serial.print(pitch_rot);
    Serial.println(" rotations...\n");
  } else {                                        // motor out of range
    pitchPos -= pitch_rot;
    Serial.println("Out of range. Home again.\n");
  }
  Serial.print("Current pitch motor position: ");
  Serial.print(pitchPos);
  Serial.print(" / 140\n");
}

void blstMotor(float blst_rot) {
  // maximum is 16 rotations!
  float blst_angle = 360.0 * blst_rot;    // 4 rotations
  float blst_steps = blst_angle / 360 * MOTOR_STEPS * MICROSTEPS * GEAR_RATIO / FACTOR;
  blstPos += blst_rot;

  if (-blst_steps <= BLST_MAX && blstPos <= 16 && blstPos >= 0) {                   // motor within range
    stepperBlst.rotate(blst_angle * GEAR_RATIO / FACTOR);
    Serial.print("Rolling ");
    Serial.print(blst_rot);
    Serial.println(" rotations...\n");
  } else {                                        // motor out of range
    blstPos -= blst_rot;
    Serial.println("Out of range. Home again.\n");
  }
  Serial.print("Current ballast motor position: ");
  Serial.print(blstPos);
  Serial.print(" / 16\n");
}
