void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);   // start the Serial monitor with speed of 9600 Bauds
  gpsSerial.begin(9600);

  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(ROLL_ENDSTOP, INPUT_PULLUP);
  pinMode(PITCH_ENDSTOP, INPUT_PULLUP);
  pinMode(BLST_ENDSTOP, INPUT_PULLUP);

  stepperRoll.begin(MOTOR_RPM / 2, MICROSTEPS);
  stepperPitch.begin(MOTOR_RPM, MICROSTEPS);
  stepperBlst.begin(MOTOR_RPM, MICROSTEPS);

  stepperRoll.setEnableActiveState(LOW);
  stepperRoll.enable();    // enable motor
  stepperPitch.setEnableActiveState(LOW);
  stepperPitch.enable();    // enable motor
  stepperBlst.setEnableActiveState(LOW);
  stepperBlst.enable();    // enable motor
}
