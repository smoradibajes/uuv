// REMEBER TO DISCONNECT PIN 0 & 1 IF UPLOADING NEW SKETCH
/* Include libraries -------------------------------------------------------------------------------------*/
#include <Arduino.h>
#include <Wire.h>

#include "BasicStepperDriver.h"                 // StepperDriver library from https://github.com/laurb9/StepperDriver 

#include "I2Cdev.h"                             // I2Cdev and MPU6050 library from https://github.com/jrowberg/i2cdevlib
#include "MPU6050_6Axis_MotionApps20.h"
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
#include "Wire.h"
#endif

#include <NeoSWSerial.h>                        // NeoSWSerial library https://github.com/SlashDevin/NeoSWSerial
#include <TinyGPS++.h>                          // TinyGPS++ library for converting GPS NMEA data to useful info https://github.com/mikalhart/TinyGPSPlus
NeoSWSerial gpsSerial(53, 52);                  // (Rx, Tx) Arduino pins. RX pin is white, TX pin is orange
TinyGPSPlus gps;                                // Create a TinyGPS++ object

/* Define variables for MPU6050 ----------------------------------------------------------------------- */
MPU6050 mpu;
#define OUTPUT_READABLE_YAWPITCHROLL
#define INTERRUPT_PIN 19
// SDA: blue wire, SCL: oragne wire
bool blinkState = false;
// MPU control/status vars
bool dmpReady = false;  // set true if DMP init was successful
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

// orientation/motion vars
Quaternion q;           // [w, x, y, z]         quaternion container
VectorInt16 aa;         // [x, y, z]            accel sensor measurements
VectorInt16 aaReal;     // [x, y, z]            gravity-free accel sensor measurements
VectorInt16 aaWorld;    // [x, y, z]            world-frame accel sensor measurements
VectorFloat gravity;    // [x, y, z]            gravity vector
float euler[3];         // [psi, theta, phi]    Euler angle container
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector

// interrupt detection routine
volatile bool mpuInterrupt = false;     // indicates whether MPU interrupt pin has gone high
void dmpDataReady() {
  mpuInterrupt = true;
}

/* Define variables for StepperDriver library -----------------------------------------------------------------------------*/
#define MOTOR_STEPS 200.0                       // Motor steps per revolution
#define FACTOR 2                                // Increase this number if you want the motor to spin faster    // 2 was good
#define OUT_RPM 60                              // RPM at output shaft. Change this value to desired output     // DEFAULT WAS 60, 90 was fast
#define GEAR_RATIO 5.18                         // Motor gearbox ratio = 5.18
#define PULLEY_RATIO 3                          // Pulley system ratio = 3             
#define MOTOR_RPM OUT_RPM*GEAR_RATIO/FACTOR     // RPM at the motor. This number seems to be limited to 200
#define MICROSTEPS 1*FACTOR                     // Micrstop should be 2, but multiplied by a factor of 2
// Acceleration and deceleration values are always in FULL steps / s^2
#define MOTOR_ACCEL 400
#define MOTOR_DECEL 400

/* Set up for stepper motors and endstops  --------------------------------------------------------------------------------*/
// Roll motor (rolling weights) (middle)
#define ROLL_EN 8                               // Enable pin (white wire)
#define ROLL_STP 9                              // Step pin (orange wire)
#define ROLL_DIR 10                             // Direction pin (blue wire)
#define ROLL_ENDSTOP 3                          // Endstop (yellow wire)
// Endstop pin needs to be an interrupt pin. On Arduino Mega, use one of the pins: 2, 3, 18, 19, 20, 21

// Pitch motor (moving weights) (left)
#define PITCH_EN 5
#define PITCH_STP 6
#define PITCH_DIR 7
#define PITCH_ENDSTOP 2

// Ballast motor (propulsion) (right)
#define BLST_EN 11
#define BLST_STP 12
#define BLST_DIR 13
#define BLST_ENDSTOP 18

// Define maximum range for roll, pitch, ballast (in steps)
#define ROLL_MAX 350.0/360*MOTOR_STEPS*MICROSTEPS*GEAR_RATIO/FACTOR*PULLEY_RATIO
float PITCH_MAX = 140.0 * MOTOR_STEPS * MICROSTEPS / FACTOR;                             // max is 140 revolutions
#define BLST_MAX 16.0*MOTOR_STEPS*MICROSTEPS*GEAR_RATIO/FACTOR                           // max is 16 revolutions                                             

BasicStepperDriver stepperRoll(MOTOR_STEPS, ROLL_DIR, ROLL_STP, ROLL_EN);
BasicStepperDriver stepperPitch(MOTOR_STEPS, PITCH_DIR, PITCH_STP, PITCH_EN);
BasicStepperDriver stepperBlst(MOTOR_STEPS, BLST_DIR, BLST_STP, BLST_EN);

// Funciton prototypes
void homeAll();
void homeRoll();
void homePitch();
void homeBlst();

void gpsInfo();
void displayInfo();
void imuInfo();
void imuSetup();

void rollMotor(float roll_angle);
void pitchMotor(float pitch_rot);
void blstMotor(float blst_rot);

void(* resetFunc) (void) = 0;                  // Declare reset function at address 0
