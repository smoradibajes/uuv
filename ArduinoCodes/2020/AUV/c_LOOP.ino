void loop() {
  Serial.println("Please select program: ");
  Serial.println("h. Home");
  Serial.println("g. Display GPS info");
  Serial.println("i. Display IMU info");
  Serial.println("m. Manual control");
  Serial.println("d. Disable motor");
  Serial.println("e. Enable motor");
  Serial.println("r. Reset");

  while (Serial.available() > 0) Serial.read(); // clear incoming serial buffer
  while (!Serial.available()) {}

  // read the incoming byte:
  char program = Serial.read();
  while (Serial.available() > 0) Serial.read(); // clear incoming serial buffer

  switch (program) {
    case 'h':
      Serial.println("Received h. Homing all motors... \n");
      homeAll();
      break;
    case 'g':
      Serial.println("Received g. Displaying GPS location info... \n");
      gpsInfo();
      break;
    case 'i':
      Serial.println("Received i. Retrieving IMU info... \n");
      imuInfo();
      break;
    case 'd':
      Serial.println("Received d. Disabling motors... \n");
      // disable motor, remeber to enable motor (option 'e') later
      stepperRoll.disable(); stepperPitch.disable(); stepperBlst.disable();
      break;
    case 'e':
      Serial.println("Received e. Enabling motors... \n");
      // enable motor
      stepperRoll.enable(); stepperPitch.enable(); stepperBlst.enable();
      break;
    case 'r':
      resetFunc();
      break;
    default:
      Serial.println("Error! Choose again.\n");
      break;
    case 'm':
      Serial.println("Received m\n");
      char manual_program = 'w';

      while (manual_program == 'w' || manual_program == 'e' || manual_program == 'r' || manual_program == 't' || manual_program == 'u' || manual_program == 'i' || manual_program == 'o' || manual_program == 'p' ||
             manual_program == 's' || manual_program == 'd' || manual_program == 'f' || manual_program == 'g' || manual_program == 'h' || manual_program == 'j' || manual_program == 'k' || manual_program == 'l' ||
             manual_program == 'x' || manual_program == 'c' || manual_program == 'v' || manual_program == 'b' || manual_program == 'n' || manual_program == 'm' ||
             manual_program == '1' || manual_program == '2' || manual_program == '3' || manual_program == '4' || manual_program == '5' || manual_program == '6') {
        Serial.println("Please select program: ");
        Serial.println("First row of keyboard for ballast motor control: ");
        Serial.println("w. -1; e. -1/2; r. -1/4; t. -1/8");
        Serial.println("u. +1/8; i. +1/4; o. +1/2; p. +1\n");
        Serial.println("Second row of keyboard for pitch motor control: ");
        Serial.println("s. -1; d. -1/2; f. -1/4; g. -1/8");
        Serial.println("h. +1/8; j. +1/4; k. +1/2; l. +1\n");
        Serial.println("Third row of keyboard for roll motor control");
        Serial.println("x. -90; c. -45; v. -22.5");
        Serial.println("b. +22.5; n. +45; m. +90\n");

        // different options for testing
        Serial.println("1. Home and move to C.O.M");
        Serial.println("2. Zigzag, 6,10,3s delay");
        Serial.println("3. Zigzag, 6,10,5s delay");
        Serial.println("4. Zigzag, 6,12,4s delay");
        Serial.println("5. Zigzag, 6,8,6s delay");
        Serial.println("6. Zigzag, 8,8,2s delay");
        Serial.println("7. Zigzag, 10,6,3s delay");
        Serial.println("8. Turn 22.5 deg and zigzag");
        Serial.println("9. Turn 45 deg and zigzag");
        Serial.println("q. Return to main menu\n");

        while (Serial.available() > 0) Serial.read(); // clear incoming serial buffer
        while (!Serial.available()) {}

        // read the incoming byte:
        manual_program = Serial.read();
        Serial.print("Received ");
        Serial.println(manual_program);

        switch (manual_program) {
          case 'w': blstMotor(-16.0); break;
          case 'e': blstMotor(-8.0); break;
          case 'r': blstMotor(-4.0); break;
          case 't': blstMotor(-2.0); break;
          case 'u': blstMotor(2.0); break;
          case 'i': blstMotor(4.0); break;
          case 'o': blstMotor(8.0); break;
          case 'p': blstMotor(16.0); break;

          case 's': pitchMotor(-140.0); break;
          case 'd': pitchMotor(-70.0); break;
          case 'f': pitchMotor(-35.0); break;
          case 'g': pitchMotor(-17.5); break;
          case 'h': pitchMotor(17.5); break;
          case 'j': pitchMotor(35.0); break;
          case 'k': pitchMotor(70.0); break;
          case 'l': pitchMotor(140.0); break;

          case 'x': rollMotor(-90.0); break;
          case 'c': rollMotor(-45.0); break;
          case 'v': rollMotor(-22.5); break;
          case 'b': rollMotor(22.5); break;
          case 'n': rollMotor(45.5); break;
          case 'm': rollMotor(90.0); break;

          // different test scenarios with different delays
          case '1':
            // home all motors and move to COM
            homeAll();
            rollMotor(90.0);
            pitchMotor(35);
            break;
          // case 2: best result so far
          case '2':
            blstMotor(12.0);
            delay(6000);
            blstMotor(-12.0);
            delay(10000);
            blstMotor(12.0);
            delay(3000);
            blstMotor(-12.0);
            break;
          case '3':
            blstMotor(12.0);
            delay(6000);
            blstMotor(-12.0);
            delay(10000);
            blstMotor(12.0);
            delay(5000);
            blstMotor(-12.0);
            break;
          case '4':
            blstMotor(12.0);
            delay(6000);
            blstMotor(-12.0);
            delay(12000);
            blstMotor(12.0);
            delay(4000);
            blstMotor(-12.0);
            break;
          case '5':
            blstMotor(12.0);
            delay(6000);
            blstMotor(-12.0);
            delay(8000);
            blstMotor(12.0);
            delay(6000);
            blstMotor(-12.0);
            break;
          case '6':
            blstMotor(12.0);
            delay(8000);
            blstMotor(-12.0);
            delay(8000);
            blstMotor(12.0);
            delay(2000);
            blstMotor(-12.0);
            break;
          case '7':
            blstMotor(12.0);
            delay(10000);
            blstMotor(-12.0);
            delay(6000);
            blstMotor(12.0);
            delay(3000);
            blstMotor(-12.0);
            break;
          default:
            Serial.println("Returning to main menu...\n");
            break;
        }
      }
      break;
  }
}
