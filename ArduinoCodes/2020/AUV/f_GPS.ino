// GPS code modified from https://lastminuteengineers.com/neo6m-gps-arduino-tutorial/
// Note: GPS needs to be outside to start receiving location data,
// might need to wait a few secons for data to start coming in,
// GPS LED will be flashing if there is location data available
void gpsInfo() {
  // This sketch displays information every time a new sentence is correctly encoded
  unsigned long startTime = millis();
  while (1) {
    unsigned long currentTime = millis();
    unsigned long elapsedTime = currentTime - startTime;
    while (gpsSerial.available() > 0) {
      if (gps.encode(gpsSerial.read())) {
        displayInfo();
      }
    }
    // If 30 seconds elapsed, and there is still no location data available, then quit
    if (elapsedTime > 30000) {
      Serial.println(elapsedTime);
      break;
    }

    // If 5000 milliseconds pass and there are no characters coming in
    // over the software serial port, show a "No GPS detected" error
    if (millis() > 5000 && gps.charsProcessed() < 10) {
      Serial.println("No GPS detected");
      break;
    }
  }
}

void displayInfo() {
  if (gps.location.isValid()) {
    Serial.print("Latitude: ");
    Serial.println(gps.location.lat(), 6);
    Serial.print("Longitude: ");
    Serial.println(gps.location.lng(), 6);
  }
  else {
    Serial.println("Location: Not Available");
  }

  Serial.print("Date: ");
  if (gps.date.isValid()) {
    Serial.print(gps.date.month());
    Serial.print("/");
    Serial.print(gps.date.day());
    Serial.print("/");
    Serial.println(gps.date.year());
  }
  else {
    Serial.println("Not Available");
  }

  // Time is displayed in GPS time. Subtract GPS time by 10 hours to get current Melbourne time
  Serial.print("GPS Time: ");
  if (gps.time.isValid()) {
    if (gps.time.hour() < 10) Serial.print(F("0"));
    Serial.print(gps.time.hour());
    Serial.print(":");
    if (gps.time.minute() < 10) Serial.print(F("0"));
    Serial.print(gps.time.minute());
    Serial.print(":");
    if (gps.time.second() < 10) Serial.print(F("0"));
    Serial.print(gps.time.second());
    Serial.print(".");
    if (gps.time.centisecond() < 10) Serial.print(F("0"));
    Serial.println(gps.time.centisecond());
  }
  else {
    Serial.println("Not Available");
  }

  Serial.println();
  Serial.println();
  delay(1000);
}
