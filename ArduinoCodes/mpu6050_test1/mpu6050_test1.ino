#include "I2Cdev.h"
#include "MPU6050_6Axis_MotionApps20.h"
//#include "MPU6050.h"
#include "Wire.h"



MPU6050 mpu;
Quaternion q;           // [w, x, y, z]         quaternion container;
uint8_t fifoBuffer[64]; // FIFO storage buffer
VectorFloat gravity;    // [x, y, z]            gravity vector
float euler[3];         // [psi, theta, phi]    Euler angle container
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector
uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)


void setup() {
  // join I2C bus (I2Cdev library doesn't do this automatically)
  #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
      Wire.begin();
  #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
      Fastwire::setup(400, true);
  #endif
  Serial.begin(115200);

// initialize device
  Serial.println("Status: Initializing I2C devices...");
  mpu.initialize();

// verify connection
  Serial.println("Status: Testing device connections...");
  Serial.println(mpu.testConnection() ? "Status: MPU6050 connection successful" : "Error: MPU6050 connection failed");

  // use the code below to change accel/gyro offset values
  devStatus = mpu.dmpInitialize();

  mpu.setXGyroOffset(220);
  mpu.setYGyroOffset(76);
  mpu.setZGyroOffset(-85);
  mpu.setZAccelOffset(1788); // 1688 factory default for my test chip
  // make sure it worked (returns 0 if so)
  if (devStatus == 0) {
      // Calibration Time: generate offsets and calibrate our MPU6050
      mpu.CalibrateAccel(6);
      mpu.CalibrateGyro(6);
//      mpu.PrintActiveOffsets();
      // turn on the DMP, now that it's ready
      mpu.setDMPEnabled(true);

      // get expected DMP packet size for later comparison
      packetSize = mpu.dmpGetFIFOPacketSize();
  }


}

void loop() {
  if (mpu.dmpGetCurrentFIFOPacket(fifoBuffer)) { // Get the Latest packet 
    // display Euler angles in degrees
    mpu.dmpGetQuaternion(&q, fifoBuffer);
    mpu.dmpGetGravity(&gravity, &q);
    mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);
//    Serial.print("\t ypr\t");
    Serial.print("Data: ");
    Serial.print(ypr[0] * 180/M_PI);
    Serial.print(" ");
    Serial.print(ypr[1] * 180/M_PI);
    Serial.print(" ");
    Serial.println(ypr[2] * 180/M_PI);
    delay(100);
  }
}
