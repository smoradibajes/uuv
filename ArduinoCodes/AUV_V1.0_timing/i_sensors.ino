void Get_pH()
{
  if(ph_counter>9)
  {
    float m =(-3.0/(ad4-ad7));
    float c = 7-(m*ad7);
    sensorValue =(currentValue / 10);
    ph  =((m*sensorValue)+c);
    ph_counter = 0;
    currentValue = 0;
  }
  currentValue += analogRead(sensorPin);
  ph_counter = ph_counter +1;
}

void Get_Pressure()
{
  V = analogRead(0) * 5.00 / 1024;     //Sensor output voltage
  P = (V - OffSet) * 400;             //Calculate water pressure

  depth = P / (1030 * 9.8); // h = p/(rho g)
  }
