#include "ros/ros.h"
#include <ros/package.h>
#include <std_msgs/String.h>

#include <iostream>
#include <fstream>
#include <string>

// Include the UUV package message types
#include "uuv_pkg/UsbReadMsg.h"
#include "uuv_pkg/UsbSendMsg.h"
// recieved data: [yaw, roll, pitch, depth, temp, ph, gps_latitude , gps_longitude]
float yaw = 0;
float roll = 0;
float pitch = 0;
float depth = 0;
float temp = 0;
float ph = 0;
float gps_latitude = 0;
float gps_longitude = 0;
float t_dure, u_ctrl, m_cyl =0;
int program = 2;

std_msgs::String stt;
std_msgs::String err;

std::string address = "/home/uuv/uuv/web_interface/results/";
std::ofstream myStatfile(address + "stat.csv");
float time_count = 0;

void usb_data_callback(const uuv_pkg::UsbReadMsg& msg)
{
	yaw = msg.yaw;
	roll = msg.roll;
	pitch = msg.pitch;
    depth = msg.depth;
    temp = msg.temp;
    ph = msg.ph;
    gps_latitude = msg.gps_latitude;
    gps_longitude = msg.gps_longitude;
    t_dure = msg.t_dure;
    u_ctrl = msg.u_ctrl;
    m_cyl = msg.m_cyl;
    program = msg.program;
}
void usb_stats_callback(const std_msgs::String& msg)
{
	stt = msg;
    myStatfile << time_count << " ; ";
    myStatfile << stt << ",";
    myStatfile << "\n"; 
}
void usb_errs_callback(const std_msgs::String& msg)
{
	err = msg;
    myStatfile << time_count << " ; ";
    myStatfile << err << ",";
    myStatfile << "\n"; 
}


int main(int argc, char* argv[])
{
	// Initialise the node
	ros::init(argc, argv, "csv");
	ros::NodeHandle nodeHandle("~");
	
	ros::Subscriber data_sub = nodeHandle.subscribe("/uuv/usb_data", 1, usb_data_callback);
	ros::Subscriber status_sub = nodeHandle.subscribe("/uuv/usb_stats", 1, usb_stats_callback);
	ros::Subscriber error_sub = nodeHandle.subscribe("/uuv/usb_errs", 1, usb_errs_callback);

    
    int freq = 10;


    ros::Rate loop_rate(freq);

    /*
    int i = 0;
    std::string line ;
    std::string str_i = "0";
    while(true){
        myStatfile(address + "stat" + str_i + ".csv");
        // check if the first line is status
        bool check = std::getline( myStatfile, line ) && (line == "status"); 
        if(!check){
            myStatfile << "status";
            myStatfile << "\n"; 
            break;
        }
        i = i +1;
        std::itoa(i, str_i, 3);
    }
*/
    std::ofstream myfile("/home/uuv/uuv/web_interface/results/result.csv");
    /*
    i = 0;
    line = "";
    str_i = "0";
    while(true){
        myfile(address + "result" + str_i + ".csv");
        // check if the first line is "time, yaw, roll, pitch, depth, temp, ph, gps_latitude , gps_longitude"
        bool check = std::getline( myfile, line ) && (line == "time, yaw, roll, pitch, depth, temp, ph, gps_latitude , gps_longitude"); 
        if(!check){
            break;
        }
        i = i +1;
        std::itoa(i, str_i, 3);
    }*/
    ROS_INFO_STREAM("csv open");
    ROS_INFO_STREAM(myfile.is_open());
    // recieved data: [yaw, roll, pitch, depth, temp, ph, gps_latitude , gps_longitude, t_dure, u_ctrl, m_cyl, program]
    myfile << "time, yaw, roll, pitch, depth, temp, ph, gps_latitude , gps_longitude, t_dure, u_ctrl, m_cyl, program"
//    myfile << "time, status, error, yaw, roll, pitch"
                "\n";
	while (ros::ok())
	{
        //ROS_INFO_STREAM("csv write");
        myfile << time_count << ",";
        //myfile << stt.data << "," << err.data << ",";
        myfile << yaw << "," << roll << "," << pitch << "," << depth << "," << temp << "," << ph << ","; 
        myfile << gps_latitude << "," << gps_longitude << "," << t_dure << "," << u_ctrl << ",";
        myfile << m_cyl << "," << program << ",";
        myfile << "\n";  
        time_count = time_count + 1.0/freq;

        ros::spinOnce();
		loop_rate.sleep();		
	}
    myfile.close();
    //ROS_INFO_STREAM("csv close");

	return 0;
}


