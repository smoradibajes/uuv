// https://github.com/garyservin/serial-example/blob/master/src/serial_example_node.cpp

#include <ros/ros.h>
#include <serial/serial.h>
#include <std_msgs/String.h>
#include <std_msgs/Empty.h>
#include <string.h>
#include <sstream>
#include <algorithm>
using namespace std;

// Include the UUV package message types
#include "uuv_pkg/UsbReadMsg.h"
#include "uuv_pkg/UsbSendMsg.h"


serial::Serial ser;
uuv_pkg::UsbReadMsg usb_data;

int mode = 0;
float ref_dep = 0;
float mnul_blst = 0;
float mnul_rol = 0;
float mnul_pitch = 0;

void write_callback(const uuv_pkg::UsbSendMsg& msg){
//    ROS_INFO_STREAM("Writing to serial port" << msg->data);
//    senddata : [mode, ref_dep, mnul_blst, mnul_rol, mnul_pitch]
//                  0        1         2         3        4
    switch (msg.var)
    {
        case 0: // mode
        mode = int(msg.value);
        break;
        case 1: // ref_dep
        ref_dep = msg.value;
        break;
        case 2: // mnul_blst
        mnul_blst = msg.value;
        break;
        case 3: // mnul_rol
        mnul_rol = msg.value;
        break;
        case 4: // mnul_pitch
        mnul_pitch = msg.value;
        break;
        default:
        break;
        // out of range
    }

}

int read_data_type (string res){
    if (strstr(res.c_str(), "Data: "))
    {
        return 1; // usb sent data 
    }
    else if (strstr(res.c_str(), "Error: "))
    {
        return -1; // usb sent error msg 
    }
    else {
        return 2; // usb sent status msg
    }
}
void extract_data (string res){
    int w = std::count(res.cbegin(), res.cend(), ' ')+1;
    // recieved data: [yaw, roll, pitch, depth, temp, ph, gps_latitude , gps_longitude, t_dure, u_ctrl, m_cyl, program]
    // word variable to store word
    if(w>12){
        try{
        string word;
        // making a string stream

        stringstream iss(res);
        iss >> word; // first word is datatype
        iss >> word; // 1st data is the yaw
        usb_data.yaw = stof(word); // convert to float and store
        iss >> word; // 2nd data is the roll
        usb_data.roll = stof(word); // convert to float and store
        iss >> word; // 3rd data is the pitch
        usb_data.pitch = stof(word); // convert to float and store
        iss >> word; // 4th data is the depth
        usb_data.depth = stof(word); // convert to float and store
        iss >> word; // 5th data is the temp
        usb_data.temp = stof(word); // convert to float and store
        iss >> word; // 6th data is the ph
        usb_data.ph = stof(word); // convert to float and store
        iss >> word; // 7th data is the gps_latitude
        usb_data.gps_latitude = stof(word); // convert to float and store
        iss >> word; // 8th data is the gps_longitude
        usb_data.gps_longitude = stof(word); // convert to float and store
        iss >> word; // 9th data is the t_dure
        usb_data.t_dure = stof(word); // convert to float and store
        iss >> word; // 10th data is the u_ctrl
        usb_data.u_ctrl = stof(word); // convert to float and store
        iss >> word; // 11th data is the m_cyl
        usb_data.m_cyl = stof(word); // convert to float and store
        iss >> word; // 12th data is the program
        usb_data.program = stoi(word); // convert to int and store
        }
        catch( ... )
        {
            return;
        }
    }
    return;
}

int main (int argc, char** argv){
    ros::init(argc, argv, "usb");
    ros::NodeHandle nh;

    ros::Subscriber write_sub = nh.subscribe("usb_write", 1, write_callback);
    ros::Publisher data_pub = nh.advertise<uuv_pkg::UsbReadMsg>("usb_data", 1);
    ros::Publisher status_pub = nh.advertise<std_msgs::String>("usb_stats", 1);
    ros::Publisher error_pub = nh.advertise<std_msgs::String>("usb_errs", 1);

    try
    {
        //ser.setPort("/dev/ttyUSB0");
        ser.setPort("/dev/ttyACM0");
        ser.setBaudrate(115200);
        serial::Timeout to = serial::Timeout::simpleTimeout(1000);
        ser.setTimeout(to);
        ser.open();
    }
    catch (serial::IOException& e)
    {
        ROS_ERROR_STREAM("Unable to open port ");
        return -1;
    }

    if(ser.isOpen()){
        ROS_INFO_STREAM("Serial Port initialized");
    }else{
        return -1;
    }

    ros::Rate loop_rate(10); // 10 hz
    ros::Duration(14).sleep(); // sleep for half a second
    while(ros::ok()){

        ros::spinOnce();

        if(ser.available()){
            //ROS_INFO_STREAM("Reading from serial port");
            std_msgs::String result;
            result.data = ser.read(ser.available());
            int type = read_data_type(result.data);
            if(type == 1) // data
            {
                extract_data(result.data);
                data_pub.publish(usb_data);
            }
            else if(type == -1) // error
            {
                error_pub.publish(result);
            }
            else // status
            {
                status_pub.publish(result);
            }
            ROS_INFO_STREAM("Read: " << result.data << type);
            //read_pub.publish(result);
        }
        
        // send data : [mode, ref_dep, mnul_blst, mnul_rol, mnul_pitch]
        // ex: 2 1.2 3.3 4.4 2.2 

        std::string usb_msg = " ";
        usb_msg = usb_msg + std::to_string(mode);
        usb_msg = usb_msg + " ";
        usb_msg=usb_msg + std::to_string(ref_dep).substr(0, std::to_string(ref_dep).find(".") + 2 + 1);
        usb_msg = usb_msg + " ";
        usb_msg=usb_msg + std::to_string(mnul_blst).substr(0, std::to_string(mnul_blst).find(".") + 2 + 1);
        usb_msg = usb_msg + " ";
        usb_msg=usb_msg + std::to_string(mnul_rol).substr(0, std::to_string(mnul_rol).find(".") + 2 + 1);
        usb_msg = usb_msg + " ";
        usb_msg=usb_msg + std::to_string(mnul_pitch).substr(0, std::to_string(mnul_pitch).find(".") + 2 + 1);
        ser.write(usb_msg);
        //ROS_INFO_STREAM(usb_msg);

        loop_rate.sleep();

    }
}
