
(cl:in-package :asdf)

(defsystem "uuv_pkg-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "UsbReadMsg" :depends-on ("_package_UsbReadMsg"))
    (:file "_package_UsbReadMsg" :depends-on ("_package"))
    (:file "UsbSendMsg" :depends-on ("_package_UsbSendMsg"))
    (:file "_package_UsbSendMsg" :depends-on ("_package"))
  ))