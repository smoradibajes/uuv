(cl:in-package uuv_pkg-msg)
(cl:export '(YAW-VAL
          YAW
          ROLL-VAL
          ROLL
          PITCH-VAL
          PITCH
          DEPTH-VAL
          DEPTH
          TEMP-VAL
          TEMP
          PH-VAL
          PH
          GPS_LATITUDE-VAL
          GPS_LATITUDE
          GPS_LONGITUDE-VAL
          GPS_LONGITUDE
          T_DURE-VAL
          T_DURE
          U_CTRL-VAL
          U_CTRL
          M_CYL-VAL
          M_CYL
          PROGRAM-VAL
          PROGRAM
))