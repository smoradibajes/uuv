; Auto-generated. Do not edit!


(cl:in-package uuv_pkg-msg)


;//! \htmlinclude UsbSendMsg.msg.html

(cl:defclass <UsbSendMsg> (roslisp-msg-protocol:ros-message)
  ((var
    :reader var
    :initarg :var
    :type cl:integer
    :initform 0)
   (value
    :reader value
    :initarg :value
    :type cl:float
    :initform 0.0))
)

(cl:defclass UsbSendMsg (<UsbSendMsg>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <UsbSendMsg>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'UsbSendMsg)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name uuv_pkg-msg:<UsbSendMsg> is deprecated: use uuv_pkg-msg:UsbSendMsg instead.")))

(cl:ensure-generic-function 'var-val :lambda-list '(m))
(cl:defmethod var-val ((m <UsbSendMsg>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader uuv_pkg-msg:var-val is deprecated.  Use uuv_pkg-msg:var instead.")
  (var m))

(cl:ensure-generic-function 'value-val :lambda-list '(m))
(cl:defmethod value-val ((m <UsbSendMsg>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader uuv_pkg-msg:value-val is deprecated.  Use uuv_pkg-msg:value instead.")
  (value m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <UsbSendMsg>) ostream)
  "Serializes a message object of type '<UsbSendMsg>"
  (cl:let* ((signed (cl:slot-value msg 'var)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'value))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <UsbSendMsg>) istream)
  "Deserializes a message object of type '<UsbSendMsg>"
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'var) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'value) (roslisp-utils:decode-double-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<UsbSendMsg>)))
  "Returns string type for a message object of type '<UsbSendMsg>"
  "uuv_pkg/UsbSendMsg")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'UsbSendMsg)))
  "Returns string type for a message object of type 'UsbSendMsg"
  "uuv_pkg/UsbSendMsg")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<UsbSendMsg>)))
  "Returns md5sum for a message object of type '<UsbSendMsg>"
  "e9bb7d014041fc65867a06541bfe80b4")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'UsbSendMsg)))
  "Returns md5sum for a message object of type 'UsbSendMsg"
  "e9bb7d014041fc65867a06541bfe80b4")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<UsbSendMsg>)))
  "Returns full string definition for message of type '<UsbSendMsg>"
  (cl:format cl:nil "int32 var~%float64 value~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'UsbSendMsg)))
  "Returns full string definition for message of type 'UsbSendMsg"
  (cl:format cl:nil "int32 var~%float64 value~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <UsbSendMsg>))
  (cl:+ 0
     4
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <UsbSendMsg>))
  "Converts a ROS message object to a list"
  (cl:list 'UsbSendMsg
    (cl:cons ':var (var msg))
    (cl:cons ':value (value msg))
))
