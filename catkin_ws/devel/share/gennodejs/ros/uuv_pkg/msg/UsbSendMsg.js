// Auto-generated. Do not edit!

// (in-package uuv_pkg.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class UsbSendMsg {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.var = null;
      this.value = null;
    }
    else {
      if (initObj.hasOwnProperty('var')) {
        this.var = initObj.var
      }
      else {
        this.var = 0;
      }
      if (initObj.hasOwnProperty('value')) {
        this.value = initObj.value
      }
      else {
        this.value = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type UsbSendMsg
    // Serialize message field [var]
    bufferOffset = _serializer.int32(obj.var, buffer, bufferOffset);
    // Serialize message field [value]
    bufferOffset = _serializer.float64(obj.value, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type UsbSendMsg
    let len;
    let data = new UsbSendMsg(null);
    // Deserialize message field [var]
    data.var = _deserializer.int32(buffer, bufferOffset);
    // Deserialize message field [value]
    data.value = _deserializer.float64(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 12;
  }

  static datatype() {
    // Returns string type for a message object
    return 'uuv_pkg/UsbSendMsg';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'e9bb7d014041fc65867a06541bfe80b4';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    int32 var
    float64 value
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new UsbSendMsg(null);
    if (msg.var !== undefined) {
      resolved.var = msg.var;
    }
    else {
      resolved.var = 0
    }

    if (msg.value !== undefined) {
      resolved.value = msg.value;
    }
    else {
      resolved.value = 0.0
    }

    return resolved;
    }
};

module.exports = UsbSendMsg;
