// Auto-generated. Do not edit!

// (in-package uuv_pkg.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class UsbReadMsg {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.yaw = null;
      this.roll = null;
      this.pitch = null;
      this.depth = null;
      this.temp = null;
      this.ph = null;
      this.gps_latitude = null;
      this.gps_longitude = null;
      this.t_dure = null;
      this.u_ctrl = null;
      this.m_cyl = null;
      this.program = null;
    }
    else {
      if (initObj.hasOwnProperty('yaw')) {
        this.yaw = initObj.yaw
      }
      else {
        this.yaw = 0.0;
      }
      if (initObj.hasOwnProperty('roll')) {
        this.roll = initObj.roll
      }
      else {
        this.roll = 0.0;
      }
      if (initObj.hasOwnProperty('pitch')) {
        this.pitch = initObj.pitch
      }
      else {
        this.pitch = 0.0;
      }
      if (initObj.hasOwnProperty('depth')) {
        this.depth = initObj.depth
      }
      else {
        this.depth = 0.0;
      }
      if (initObj.hasOwnProperty('temp')) {
        this.temp = initObj.temp
      }
      else {
        this.temp = 0.0;
      }
      if (initObj.hasOwnProperty('ph')) {
        this.ph = initObj.ph
      }
      else {
        this.ph = 0.0;
      }
      if (initObj.hasOwnProperty('gps_latitude')) {
        this.gps_latitude = initObj.gps_latitude
      }
      else {
        this.gps_latitude = 0.0;
      }
      if (initObj.hasOwnProperty('gps_longitude')) {
        this.gps_longitude = initObj.gps_longitude
      }
      else {
        this.gps_longitude = 0.0;
      }
      if (initObj.hasOwnProperty('t_dure')) {
        this.t_dure = initObj.t_dure
      }
      else {
        this.t_dure = 0.0;
      }
      if (initObj.hasOwnProperty('u_ctrl')) {
        this.u_ctrl = initObj.u_ctrl
      }
      else {
        this.u_ctrl = 0.0;
      }
      if (initObj.hasOwnProperty('m_cyl')) {
        this.m_cyl = initObj.m_cyl
      }
      else {
        this.m_cyl = 0.0;
      }
      if (initObj.hasOwnProperty('program')) {
        this.program = initObj.program
      }
      else {
        this.program = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type UsbReadMsg
    // Serialize message field [yaw]
    bufferOffset = _serializer.float64(obj.yaw, buffer, bufferOffset);
    // Serialize message field [roll]
    bufferOffset = _serializer.float64(obj.roll, buffer, bufferOffset);
    // Serialize message field [pitch]
    bufferOffset = _serializer.float64(obj.pitch, buffer, bufferOffset);
    // Serialize message field [depth]
    bufferOffset = _serializer.float64(obj.depth, buffer, bufferOffset);
    // Serialize message field [temp]
    bufferOffset = _serializer.float64(obj.temp, buffer, bufferOffset);
    // Serialize message field [ph]
    bufferOffset = _serializer.float64(obj.ph, buffer, bufferOffset);
    // Serialize message field [gps_latitude]
    bufferOffset = _serializer.float64(obj.gps_latitude, buffer, bufferOffset);
    // Serialize message field [gps_longitude]
    bufferOffset = _serializer.float64(obj.gps_longitude, buffer, bufferOffset);
    // Serialize message field [t_dure]
    bufferOffset = _serializer.float64(obj.t_dure, buffer, bufferOffset);
    // Serialize message field [u_ctrl]
    bufferOffset = _serializer.float64(obj.u_ctrl, buffer, bufferOffset);
    // Serialize message field [m_cyl]
    bufferOffset = _serializer.float64(obj.m_cyl, buffer, bufferOffset);
    // Serialize message field [program]
    bufferOffset = _serializer.int32(obj.program, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type UsbReadMsg
    let len;
    let data = new UsbReadMsg(null);
    // Deserialize message field [yaw]
    data.yaw = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [roll]
    data.roll = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [pitch]
    data.pitch = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [depth]
    data.depth = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [temp]
    data.temp = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [ph]
    data.ph = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [gps_latitude]
    data.gps_latitude = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [gps_longitude]
    data.gps_longitude = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [t_dure]
    data.t_dure = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [u_ctrl]
    data.u_ctrl = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [m_cyl]
    data.m_cyl = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [program]
    data.program = _deserializer.int32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 92;
  }

  static datatype() {
    // Returns string type for a message object
    return 'uuv_pkg/UsbReadMsg';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '6543420b7664d3a4c4dcde74272bc593';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float64 yaw
    float64 roll
    float64 pitch
    float64 depth
    float64 temp
    float64 ph
    float64 gps_latitude
    float64 gps_longitude
    float64 t_dure
    float64 u_ctrl
    float64 m_cyl
    int32 program
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new UsbReadMsg(null);
    if (msg.yaw !== undefined) {
      resolved.yaw = msg.yaw;
    }
    else {
      resolved.yaw = 0.0
    }

    if (msg.roll !== undefined) {
      resolved.roll = msg.roll;
    }
    else {
      resolved.roll = 0.0
    }

    if (msg.pitch !== undefined) {
      resolved.pitch = msg.pitch;
    }
    else {
      resolved.pitch = 0.0
    }

    if (msg.depth !== undefined) {
      resolved.depth = msg.depth;
    }
    else {
      resolved.depth = 0.0
    }

    if (msg.temp !== undefined) {
      resolved.temp = msg.temp;
    }
    else {
      resolved.temp = 0.0
    }

    if (msg.ph !== undefined) {
      resolved.ph = msg.ph;
    }
    else {
      resolved.ph = 0.0
    }

    if (msg.gps_latitude !== undefined) {
      resolved.gps_latitude = msg.gps_latitude;
    }
    else {
      resolved.gps_latitude = 0.0
    }

    if (msg.gps_longitude !== undefined) {
      resolved.gps_longitude = msg.gps_longitude;
    }
    else {
      resolved.gps_longitude = 0.0
    }

    if (msg.t_dure !== undefined) {
      resolved.t_dure = msg.t_dure;
    }
    else {
      resolved.t_dure = 0.0
    }

    if (msg.u_ctrl !== undefined) {
      resolved.u_ctrl = msg.u_ctrl;
    }
    else {
      resolved.u_ctrl = 0.0
    }

    if (msg.m_cyl !== undefined) {
      resolved.m_cyl = msg.m_cyl;
    }
    else {
      resolved.m_cyl = 0.0
    }

    if (msg.program !== undefined) {
      resolved.program = msg.program;
    }
    else {
      resolved.program = 0
    }

    return resolved;
    }
};

module.exports = UsbReadMsg;
