;; Auto-generated. Do not edit!


(when (boundp 'uuv_pkg::UsbReadMsg)
  (if (not (find-package "UUV_PKG"))
    (make-package "UUV_PKG"))
  (shadow 'UsbReadMsg (find-package "UUV_PKG")))
(unless (find-package "UUV_PKG::USBREADMSG")
  (make-package "UUV_PKG::USBREADMSG"))

(in-package "ROS")
;;//! \htmlinclude UsbReadMsg.msg.html


(defclass uuv_pkg::UsbReadMsg
  :super ros::object
  :slots (_yaw _roll _pitch _depth _temp _ph _gps_latitude _gps_longitude _t_dure _u_ctrl _m_cyl _program ))

(defmethod uuv_pkg::UsbReadMsg
  (:init
   (&key
    ((:yaw __yaw) 0.0)
    ((:roll __roll) 0.0)
    ((:pitch __pitch) 0.0)
    ((:depth __depth) 0.0)
    ((:temp __temp) 0.0)
    ((:ph __ph) 0.0)
    ((:gps_latitude __gps_latitude) 0.0)
    ((:gps_longitude __gps_longitude) 0.0)
    ((:t_dure __t_dure) 0.0)
    ((:u_ctrl __u_ctrl) 0.0)
    ((:m_cyl __m_cyl) 0.0)
    ((:program __program) 0)
    )
   (send-super :init)
   (setq _yaw (float __yaw))
   (setq _roll (float __roll))
   (setq _pitch (float __pitch))
   (setq _depth (float __depth))
   (setq _temp (float __temp))
   (setq _ph (float __ph))
   (setq _gps_latitude (float __gps_latitude))
   (setq _gps_longitude (float __gps_longitude))
   (setq _t_dure (float __t_dure))
   (setq _u_ctrl (float __u_ctrl))
   (setq _m_cyl (float __m_cyl))
   (setq _program (round __program))
   self)
  (:yaw
   (&optional __yaw)
   (if __yaw (setq _yaw __yaw)) _yaw)
  (:roll
   (&optional __roll)
   (if __roll (setq _roll __roll)) _roll)
  (:pitch
   (&optional __pitch)
   (if __pitch (setq _pitch __pitch)) _pitch)
  (:depth
   (&optional __depth)
   (if __depth (setq _depth __depth)) _depth)
  (:temp
   (&optional __temp)
   (if __temp (setq _temp __temp)) _temp)
  (:ph
   (&optional __ph)
   (if __ph (setq _ph __ph)) _ph)
  (:gps_latitude
   (&optional __gps_latitude)
   (if __gps_latitude (setq _gps_latitude __gps_latitude)) _gps_latitude)
  (:gps_longitude
   (&optional __gps_longitude)
   (if __gps_longitude (setq _gps_longitude __gps_longitude)) _gps_longitude)
  (:t_dure
   (&optional __t_dure)
   (if __t_dure (setq _t_dure __t_dure)) _t_dure)
  (:u_ctrl
   (&optional __u_ctrl)
   (if __u_ctrl (setq _u_ctrl __u_ctrl)) _u_ctrl)
  (:m_cyl
   (&optional __m_cyl)
   (if __m_cyl (setq _m_cyl __m_cyl)) _m_cyl)
  (:program
   (&optional __program)
   (if __program (setq _program __program)) _program)
  (:serialization-length
   ()
   (+
    ;; float64 _yaw
    8
    ;; float64 _roll
    8
    ;; float64 _pitch
    8
    ;; float64 _depth
    8
    ;; float64 _temp
    8
    ;; float64 _ph
    8
    ;; float64 _gps_latitude
    8
    ;; float64 _gps_longitude
    8
    ;; float64 _t_dure
    8
    ;; float64 _u_ctrl
    8
    ;; float64 _m_cyl
    8
    ;; int32 _program
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64 _yaw
       (sys::poke _yaw (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _roll
       (sys::poke _roll (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _pitch
       (sys::poke _pitch (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _depth
       (sys::poke _depth (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _temp
       (sys::poke _temp (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _ph
       (sys::poke _ph (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _gps_latitude
       (sys::poke _gps_latitude (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _gps_longitude
       (sys::poke _gps_longitude (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _t_dure
       (sys::poke _t_dure (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _u_ctrl
       (sys::poke _u_ctrl (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _m_cyl
       (sys::poke _m_cyl (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; int32 _program
       (write-long _program s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64 _yaw
     (setq _yaw (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _roll
     (setq _roll (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _pitch
     (setq _pitch (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _depth
     (setq _depth (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _temp
     (setq _temp (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _ph
     (setq _ph (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _gps_latitude
     (setq _gps_latitude (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _gps_longitude
     (setq _gps_longitude (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _t_dure
     (setq _t_dure (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _u_ctrl
     (setq _u_ctrl (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _m_cyl
     (setq _m_cyl (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; int32 _program
     (setq _program (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get uuv_pkg::UsbReadMsg :md5sum-) "6543420b7664d3a4c4dcde74272bc593")
(setf (get uuv_pkg::UsbReadMsg :datatype-) "uuv_pkg/UsbReadMsg")
(setf (get uuv_pkg::UsbReadMsg :definition-)
      "float64 yaw
float64 roll
float64 pitch
float64 depth
float64 temp
float64 ph
float64 gps_latitude
float64 gps_longitude
float64 t_dure
float64 u_ctrl
float64 m_cyl
int32 program
")



(provide :uuv_pkg/UsbReadMsg "6543420b7664d3a4c4dcde74272bc593")


