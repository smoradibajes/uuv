;; Auto-generated. Do not edit!


(when (boundp 'uuv_pkg::UsbSendMsg)
  (if (not (find-package "UUV_PKG"))
    (make-package "UUV_PKG"))
  (shadow 'UsbSendMsg (find-package "UUV_PKG")))
(unless (find-package "UUV_PKG::USBSENDMSG")
  (make-package "UUV_PKG::USBSENDMSG"))

(in-package "ROS")
;;//! \htmlinclude UsbSendMsg.msg.html


(defclass uuv_pkg::UsbSendMsg
  :super ros::object
  :slots (_var _value ))

(defmethod uuv_pkg::UsbSendMsg
  (:init
   (&key
    ((:var __var) 0)
    ((:value __value) 0.0)
    )
   (send-super :init)
   (setq _var (round __var))
   (setq _value (float __value))
   self)
  (:var
   (&optional __var)
   (if __var (setq _var __var)) _var)
  (:value
   (&optional __value)
   (if __value (setq _value __value)) _value)
  (:serialization-length
   ()
   (+
    ;; int32 _var
    4
    ;; float64 _value
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int32 _var
       (write-long _var s)
     ;; float64 _value
       (sys::poke _value (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int32 _var
     (setq _var (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; float64 _value
     (setq _value (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;;
   self)
  )

(setf (get uuv_pkg::UsbSendMsg :md5sum-) "e9bb7d014041fc65867a06541bfe80b4")
(setf (get uuv_pkg::UsbSendMsg :datatype-) "uuv_pkg/UsbSendMsg")
(setf (get uuv_pkg::UsbSendMsg :definition-)
      "int32 var
float64 value
")



(provide :uuv_pkg/UsbSendMsg "e9bb7d014041fc65867a06541bfe80b4")


